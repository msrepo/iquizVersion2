
package com.iquiz;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.CompoundBorder;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;

public class Zze extends JFrame {

	private JPanel contentPane;
	public  JLabel tim = new JLabel("");
	String name;
	
	PlayGame pg = new PlayGame();
	int sec =  pg.seconds;
	int time = sec;
	Timer timer = new  Timer();
	TimerTask task = new TimerTask() {

		public void run() {
				tim.setText(Integer.toString(time));
				time--;
				if(time<=-1) {
					cancel();
					JOptionPane.showMessageDialog(null, "Time up", "",  JOptionPane.INFORMATION_MESSAGE);
					dispose();
					Scores sc = new Scores();
					sc.setVisible(true);
					//sc.initComponents(name);
					
				}
		}
		
	};
	public void start() {
		timer.schedule(task, 1000,1000);
	}
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Zze frame = new Zze();
					//frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Zze() {
	}
	public void initComponents(String user) {
		setVisible(true);
		name = user;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 784, 88);
		panel.setBackground(new Color(100, 149, 237));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblPlaying = new JLabel("playing: "+user);
		lblPlaying.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPlaying.setForeground(Color.WHITE);
		lblPlaying.setBounds(10, 61, 146, 16);
		panel.add(lblPlaying);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 136, 784, 325);
		panel_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_1.setBackground(new Color(255, 255, 255));
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnNewButton = new JButton("option1");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(100, 149, 237));
		btnNewButton.setBounds(10, 151, 287, 36);
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("option3");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setBackground(new Color(100, 149, 237));
		btnNewButton_1.setBounds(10, 216, 287, 36);
		panel_1.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("option2");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton_2.setForeground(new Color(255, 255, 255));
		btnNewButton_2.setBackground(new Color(100, 149, 237));
		btnNewButton_2.setBounds(487, 151, 287, 36);
		panel_1.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("option4");
		btnNewButton_3.setForeground(new Color(255, 255, 255));
		btnNewButton_3.setBackground(new Color(100, 149, 237));
		btnNewButton_3.setBounds(487, 216, 287, 36);
		panel_1.add(btnNewButton_3);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_2.setBounds(10, 26, 764, 77);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblSomeOfThe = new JLabel("Some of the  quiz questions");
		lblSomeOfThe.setBounds(0, 0, 764, 77);
		panel_2.add(lblSomeOfThe);
		lblSomeOfThe.setFont(new Font("Rockwell", Font.PLAIN, 20));
		lblSomeOfThe.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton button = new JButton("Next");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(100, 149, 237));
		button.setBounds(487, 278, 287, 36);
		panel_1.add(button);
		
		
		tim.setBounds(384, 111, 46, 14);
		contentPane.add(tim);
		
	}
}
	

