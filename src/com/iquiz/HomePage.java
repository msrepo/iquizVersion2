package com.iquiz;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HomePage extends JFrame {
	String adminname;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomePage frame = new HomePage();
					//frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public HomePage() {
	}
	public void  initComponents(String admin) {
		setVisible(true);
		adminname = admin;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800,500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 784, 148);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(339, 11, 107, 83);
		panel.add(label);
		
		JLabel lblWelcome = new JLabel("Welcome:"+admin);
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 30));
		lblWelcome.setBounds(160, 93, 469, 44);
		panel.add(lblWelcome);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(0, 0, 784, 464);
		panel.add(label_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 784, 464);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(0, 0, 46, 14);
		panel_2.add(label_2);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(100, 149, 237));
		panel_1.setBounds(0, 196, 784, 314);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnAddQuestions = new JButton("Add Questions");
		btnAddQuestions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Addpage frame1 = new Addpage();
				frame1.initComponents(admin);
				//frame1.setVisible(true);
			}
		});
		btnAddQuestions.setBackground(new Color(255, 255, 255));
		btnAddQuestions.setForeground(new Color(100, 149, 237));
		btnAddQuestions.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnAddQuestions.setBounds(232, 89, 321, 23);
		panel_1.add(btnAddQuestions);
		
		JButton btnQuestion = new JButton("List of Questions");
		btnQuestion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				Editingpage frame = new Editingpage();
				frame.initComponents(admin);
				//frame.setVisible(true);
			}
		});
		btnQuestion.setBackground(new Color(255, 255, 255));
		btnQuestion.setForeground(new Color(100, 149, 237));
		btnQuestion.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnQuestion.setBounds(232, 124, 321, 23);
		panel_1.add(btnQuestion);
		
		JButton btnSignOut_1 = new JButton("Sign out");
		btnSignOut_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				//AloginPage frame = new HomePage();
				//frame.main(null);
				/*AloginPage frame = new AloginPage();
				frame.setVisible(true);*/
				WelcomeScreen frame;
				try {
					frame = new WelcomeScreen();
					frame.setVisible(true);
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		btnSignOut_1.setHorizontalAlignment(SwingConstants.CENTER);
		btnSignOut_1.setForeground(new Color(100, 149, 237));
		btnSignOut_1.setEnabled(true);
		btnSignOut_1.setBackground(new Color(255, 255, 255));
		btnSignOut_1.setBounds(339, 173, 103, 23);
		panel_1.add(btnSignOut_1);
	}
}
