package com.iquiz;


import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Addpage extends JFrame {
	String adminname;
	private JPanel contentPane;
	private JTextField txtOption;
	private JTextField txtChoice;
	private JTextField txtChoice_1;
	private JTextField txtChoice_2;
	private JTextField txtCorrectAnswer;
	private JTextField txtEnterCatagory;
	private JLabel lblAnswer;
	private JLabel lblCategory;
	private JButton btnAdd;
	private JButton btnReset;
	private JButton btnReturn;
	
	
	Statement stmt;
	Connection conn;
	private JTextArea txtrEnterSomeQuestions;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Addpage frame1 = new Addpage();
					frame1.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Addpage() {
	}
	public void initComponents(String admin) {
		setVisible(true);
		adminname =  admin;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800,500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(0, 0, 784, 119);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAddQuestions = new JLabel("Add Questions");
		lblAddQuestions.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddQuestions.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 25));
		lblAddQuestions.setForeground(new Color(100, 149, 237));
		lblAddQuestions.setBounds(306, 11, 168, 74);
		panel.add(lblAddQuestions);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(100, 149, 237));
		panel_1.setBounds(0, 159, 784, 302);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		txtOption = new JTextField();
		txtOption.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				txtOption.setText("");
			}
		});
		txtOption.setText("Choice1");
		txtOption.setBounds(121, 130, 199, 20);
		panel_1.add(txtOption);
		txtOption.setColumns(10);
		
		txtChoice = new JTextField();
		txtChoice.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtChoice.setText("");
			}
		});
		txtChoice.setText("Choice3");
		txtChoice.setColumns(10);
		txtChoice.setBounds(121, 172, 199, 20);
		panel_1.add(txtChoice);
		
		txtChoice_1 = new JTextField();
		txtChoice_1.addMouseListener(new MouseAdapter() {
		
			public void mouseClicked(MouseEvent arg0) {
				txtChoice_1.setText("");
			}
		});
		txtChoice_1.setText("Choice2");
		txtChoice_1.setColumns(10);
		txtChoice_1.setBounds(469, 130, 199, 20);
		panel_1.add(txtChoice_1);
		
		txtChoice_2 = new JTextField();
		txtChoice_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtChoice_2.setText("");
			}
		});
		txtChoice_2.setText("Choice4");
		txtChoice_2.setColumns(10);
		txtChoice_2.setBounds(469, 172, 199, 20);
		panel_1.add(txtChoice_2);
		
		txtCorrectAnswer = new JTextField();
		txtCorrectAnswer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtCorrectAnswer.setText("");
			}
		});
		txtCorrectAnswer.setText("Correct Answer");
		txtCorrectAnswer.setColumns(10);
		txtCorrectAnswer.setBounds(121, 213, 199, 20);
		panel_1.add(txtCorrectAnswer);
		
		txtEnterCatagory = new JTextField();
		txtEnterCatagory.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtEnterCatagory.setText("");
			}
		});
		txtEnterCatagory.setText("Enter catagory");
		txtEnterCatagory.setColumns(10);
		txtEnterCatagory.setBounds(469, 213, 199, 20);
		panel_1.add(txtEnterCatagory);
		
		lblAnswer = new JLabel("Answer:");
		lblAnswer.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAnswer.setHorizontalAlignment(SwingConstants.CENTER);
		lblAnswer.setBounds(50, 215, 61, 14);
		panel_1.add(lblAnswer);
		
		lblCategory = new JLabel("Category:");
		lblCategory.setHorizontalAlignment(SwingConstants.CENTER);
		lblCategory.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblCategory.setBounds(380, 212, 80, 20);
		panel_1.add(lblCategory);
		
		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String opt1,opt2,opt3,opt4,ans,catg,qstn;
				qstn = txtrEnterSomeQuestions.getText();
				opt1 = txtOption.getText();
				opt2 = txtChoice.getText();
				opt3 = txtChoice_1.getText();
				opt4 = txtChoice_2.getText();
				ans = txtCorrectAnswer.getText();
				catg = txtEnterCatagory.getText();
				
				/// blank validation for adding question
				if(qstn.equals("") && opt1.equals("") && opt2.equals("")&& opt3.equals("") && opt4.equals("") &&  ans.equals("") && catg.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty fields","Error",JOptionPane.INFORMATION_MESSAGE);
					
				}
				else if(qstn.equals("") || opt1.equals("") || opt2.equals("") || opt3.equals("") || opt4.equals("") ||  ans.equals("") ||  catg.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty fields","Error",JOptionPane.INFORMATION_MESSAGE);
					
				}
				/*if(qstn.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}
				if(opt1.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}if( opt2.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}
				if(opt3.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}
				if(opt4.equals("") ) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}
				if(ans.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}
				if( catg.equals("")) {
					JOptionPane.showMessageDialog(null,"Enter the empty field","Error",JOptionPane.INFORMATION_MESSAGE);
				}*/
				else {
				
					try {
						Class.forName("com.mysql.cj.jdbc.Driver");
						conn = DriverManager.getConnection("jdbc:mysql://localhost:3307/Quiz","root","");
						stmt = conn.createStatement();
						stmt.executeUpdate("INSERT INTO Questions (Question,Choice1,Choice2,Choice3,Choice4,Answer,Category) VALUES ('"+txtrEnterSomeQuestions.getText()+"','"+txtOption.getText()+"','"+txtChoice.getText()+"','"+txtChoice_1.getText()+"','"+txtChoice_2.getText()+"','"+txtCorrectAnswer.getText()+"','"+txtEnterCatagory.getText()+"')");
						JOptionPane.showMessageDialog(null,"Succesfully added","Question addition",JOptionPane.INFORMATION_MESSAGE);
						
						
						//After adding the question to database setting the values back to null on the textFields
						txtrEnterSomeQuestions.setText("");
						txtOption.setText("");
						txtChoice.setText("");
						txtChoice_1.setText("");
						txtChoice_2.setText("");
						txtCorrectAnswer.setText("");
						txtEnterCatagory.setText("");
									
						
					}
					catch(Exception ex) {
						System.out.println(ex);
					}
				}
			}
		});
		btnAdd.setBounds(243, 255, 89, 23);
		panel_1.add(btnAdd);
		
		btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtrEnterSomeQuestions.setText("");
				txtOption.setText("");
				txtChoice.setText("");
				txtChoice_1.setText("");
				txtChoice_2.setText("");
				txtCorrectAnswer.setText("");
				txtEnterCatagory.setText("");
							
				
			}
		});
		btnReset.setBounds(342, 255, 89, 23);
		panel_1.add(btnReset);
		
		btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				HomePage frame = new HomePage();
				frame.setVisible(true);
				frame.initComponents(adminname);
				
			}
		});
		btnReturn.setBounds(441, 255, 89, 23);
		panel_1.add(btnReturn);
		
		txtrEnterSomeQuestions = new JTextArea();
		txtrEnterSomeQuestions.setToolTipText("");
		txtrEnterSomeQuestions.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtrEnterSomeQuestions.setText("");
			}
		});
		txtrEnterSomeQuestions.setLineWrap(true);
		txtrEnterSomeQuestions.setText("Enter some  questions  here!!!");
		
		txtrEnterSomeQuestions.setBounds(10, 11, 764, 78);
		panel_1.add(txtrEnterSomeQuestions);
	}


}
