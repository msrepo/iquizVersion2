package com.iquiz;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

public class PlayGame extends JFrame {

	private JPanel contentPane;
	static int seconds=60;
	
	
	public static void main(String[] args) {
		System.out.println(seconds);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlayGame frame = new PlayGame();
				//	frame.setVisible(true);
					//frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public PlayGame() {
		
	}
	
	  	public void initComponents(String user) {
	  		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 790, 490);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 784, 461);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(307, 39, 169, 155);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setIcon(new ImageIcon("C:\\Users\\DELL\\Desktop\\images (2).png"));
		panel.add(label_1);
		
		JLabel lblWelcome = new JLabel("Welcome: "+user);
		lblWelcome.setBounds(57, 205, 690, 79);
		lblWelcome.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcome.setForeground(new Color(240, 255, 255));
		lblWelcome.setFont(new Font("Script MT Bold", Font.PLAIN, 40));
		panel.add(lblWelcome);
		
		JButton btnPlayGame = new JButton("Play Game");
		btnPlayGame.setBounds(292, 339, 227, 41);
		btnPlayGame.setForeground(new Color(0, 0, 255));
		btnPlayGame.setFont(new Font("Goudy Old Style", Font.PLAIN, 20));
		btnPlayGame.addActionListener(new ActionListener() {

			
			public void actionPerformed(ActionEvent ae) {
				GameWindow gw = new GameWindow();
				
				gw.setVisible(true);
				setVisible(false);		
				gw.start();
				gw.initComponents(user);
				gw.setLocationRelativeTo(null);
			}
			
		});
		panel.add(btnPlayGame);
		
		Choice choice = new Choice();
		choice.setBounds(358, 299, 84, 20);
		choice.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
				if(choice.getSelectedItem() == "1 minute"	) {
					seconds = 60;
				}
				else if(choice.getSelectedItem() == "2 minute") {
					 seconds = 120;
				}
				else {
					 seconds =  180;
				}
			}
		});
		choice.add("1 minute");
		choice.add("2 minute");
		choice.add("3 minute");
		panel.add(choice);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(190, 96, 419, 317);
		panel_1.setBackground(new Color(0,0,0,15));		
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 784, 461);
		label.setForeground(new Color(245, 255, 250));
		label.setIcon(new ImageIcon("C:\\Users\\DELL\\Downloads\\1 (1).jpg"));
		panel.add(label);
	}
}
