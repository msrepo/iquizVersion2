package com.iquiz;

import java.sql.*;
//error in this page
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Scores extends JFrame {

	private JPanel contentPane;
	private JTable table;
	Statement stmt;
	Connection conn;
	ResultSet rs;
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		//UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Scores frame = new Scores();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Scores() {
	}
	
	public void initComponents(String user, int points, int timeValue) throws Exception{
		
		setVisible(true);
		
		try {

			Class.forName("com.mysql.cj.jdbc.Driver");
        	conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Quiz","root","");

            
        	
		}
		catch(Exception e) {
			
		}
		
		System.out.println(timeValue);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800,500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(0, 0, 784, 478);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBounds(0, 0, 784, 66);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblLeadershipBoard = new JLabel("Leadership Board");
		lblLeadershipBoard.setForeground(new Color(30, 144, 255));
		lblLeadershipBoard.setFont(new Font("Script MT Bold", Font.BOLD, 30));
		lblLeadershipBoard.setHorizontalAlignment(SwingConstants.CENTER);
		lblLeadershipBoard.setBounds(263, 11, 271, 44);
		panel_1.add(lblLeadershipBoard);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_2.setBackground(new Color(100, 149, 237));
		panel_2.setBounds(0, 120, 778, 359);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(116, 6, 529, 313);
		scrollPane.setBackground(Color.white);
		panel_2.add(scrollPane);
		
		//ArrayList <I_values>users  = new ArrayList<I_values>();
		ArrayList <ForScore> users = new ArrayList<ForScore>();
		//Object of the forScore type
		ForScore obj;
		 try {
	        	
	        	stmt =  conn.createStatement();
	            rs = stmt.executeQuery("SELECT * FROM Leaderboard  ORDER BY Score DESC,Time DESC; ");
	            
	            while(rs.next()){
	                
	            	//System.out.println(""+rs.getString("Username")+""+rs.getInt("Score")+""+rs.getInt("Time"));
	                obj = new ForScore(rs.getString("Username"),rs.getInt("Score"),rs.getInt("Time"));
	                users.add(obj);
	            }         
	        } catch (SQLException ex) {  
	        	
	        	
	        }
		
      String[] columnsName = new String[3];
     /* columnsName[0] = "S.no";
      columnsName[1] = "Player's Name";
      columnsName[2] = "Scores";
      */
      columnsName[0] = "Player's name";
      columnsName[1] = "Score";
      columnsName[2] = "Time Left";
      Object[][] rowData = new Object[users.size()][3];
      
      for(int p = 0; p <users.size(); p++){
          rowData[p][0] = users.get(p).uname;
          rowData[p][1] = users.get(p).time;
          rowData[p][2] = users.get(p).score;
          
             
      }
		
		table = new JTable();
		table.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		table.setBackground(Color.WHITE);
		table.setRowSelectionAllowed(false);
		table.setModel(new DefaultTableModel(rowData,columnsName) {
			boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(300);
		table.getColumnModel().getColumn(1).setPreferredWidth(70);
		table.getColumnModel().getColumn(2).setPreferredWidth(70);
		table.getTableHeader().setResizingAllowed(false);
		table.getTableHeader().setReorderingAllowed(false);
	  
		scrollPane.setViewportView(table);
		
		JButton btnPlayAgain = new JButton("Play Again");
		btnPlayAgain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				PlayGame frame = new PlayGame();
				frame.initComponents(user);
			}
		});
		btnPlayAgain.setBounds(514, 324, 117, 29);
		panel_2.add(btnPlayAgain);
		
		JLabel lblNewLabel = new JLabel("You scored: "+points);
		lblNewLabel.setBounds(117, 78, 190, 16);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Time Left: "+timeValue+"sec");
		lblNewLabel_1.setBounds(348, 78, 200, 16);
		panel.add(lblNewLabel_1);
		
		
		
	}


	
}

